var abi = [
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "uid",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "bytes[]",
				"name": "shares",
				"type": "bytes[]"
			}
		],
		"name": "DeadSwitch",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "uid",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "uint8",
				"name": "totShares",
				"type": "uint8"
			},
			{
				"indexed": false,
				"internalType": "uint8",
				"name": "minShares",
				"type": "uint8"
			}
		],
		"name": "NewSwitch",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "uid",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "bytes",
				"name": "share",
				"type": "bytes"
			}
		],
		"name": "ShareSent",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "uid",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "uint8",
				"name": "totShares",
				"type": "uint8"
			},
			{
				"indexed": false,
				"internalType": "uint8",
				"name": "minShares",
				"type": "uint8"
			},
			{
				"indexed": false,
				"internalType": "bytes32[]",
				"name": "sharesHash",
				"type": "bytes32[]"
			},
			{
				"indexed": false,
				"internalType": "bytes[]",
				"name": "shares",
				"type": "bytes[]"
			}
		],
		"name": "SwitchInfo",
		"type": "event"
	},
	{
		"inputs": [
			{
				"internalType": "uint8",
				"name": "totShares",
				"type": "uint8"
			},
			{
				"internalType": "uint8",
				"name": "minShares",
				"type": "uint8"
			},
			{
				"internalType": "bytes32[]",
				"name": "sharesHash",
				"type": "bytes32[]"
			}
		],
		"name": "createSwitch",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "switchId",
				"type": "uint256"
			}
		],
		"name": "getShares",
		"outputs": [
			{
				"internalType": "bytes[]",
				"name": "",
				"type": "bytes[]"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			}
		],
		"name": "getSwitchesByOwner",
		"outputs": [
			{
				"internalType": "uint256[]",
				"name": "",
				"type": "uint256[]"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "switchId",
				"type": "uint256"
			}
		],
		"name": "isSwitchDead",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"name": "ownerToSwitches",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "switchId",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "share",
				"type": "bytes"
			}
		],
		"name": "submitShare",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"name": "switchToOwner",
		"outputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"name": "switches",
		"outputs": [
			{
				"internalType": "bool",
				"name": "dead",
				"type": "bool"
			},
			{
				"internalType": "uint8",
				"name": "totShares",
				"type": "uint8"
			},
			{
				"internalType": "uint8",
				"name": "minShares",
				"type": "uint8"
			},
			{
				"internalType": "uint8",
				"name": "pubShares",
				"type": "uint8"
			}
		],
		"stateMutability": "view",
		"type": "function"
	}
]