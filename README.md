# Dead Man's Switch

In [2018](https://gitlab.com/brn1337/dead-mans-switch) I got the idea on how to implement a software dead-man-switch (DMS) in a very resilient way, in 2022 I finally got it implemented! (It didn't take me 4 years :P)

This is a PoC (no warranties), you can try the [DApp on the Sepolia testnet](https://brn1337.gitlab.io/dead-man-switch/).

## What is a dead man's switch

A software dead man's switch is designed to activate when its human creator becomes incapacitated, mainly due to death.
Some examples:
- Edward Snowden has distributed encrypted copies of his document trove to various people, and has set up some (unknown) automatic system to distribute the key, should something happen to him
- Google's Inactive Account Manager that allows designation of a third-party to take control of an inactive account if it has been inactive for a certain period of time

## What's different

I find some issues in the available implementations:

|           Problem           |           Solution           |
|:---------------------------:|:----------------------------:|
| Single point of failure     | Decentralized network        |
| Annullable                  | Immutable                    |
| Secret stored in cleartext  | Shamir's Shared Secret (SSS) |
| SSS has no share validation | Share hash to validate       |

As an effect of the solutions choosen this variation is heavily reliant of the third-parties entrusted by the creator of the DMS.

## How it works

Dead man's switch creation:

1. [[website](https://brn1337.gitlab.io/dead-man-switch/shamir.htm)] The user has a secret, which he splits using SSS into X shares, needing Y to recover the secret (X>1, Y<=X, Y>1).

```mermaid
graph TB
    A([secret]) --> B{{SSS split in 3 shares, 2 needed for decryption}}
    B --> C(share#1)
    B --> D(share#2)
    B --> E(share#3)
    C --> F{{SSS Decrypt}}
    E --> F
    F --> G([secret])
```

2. [offline] The user proceeds to send any number of shares to any third-party he wants.
3. [[website](https://brn1337.gitlab.io/dead-man-switch/)] The users creates a new dead man's switch, sending the smart contract the total/needed number of shares and their hashes

When a third-party will consider the conditions to send the share fullfilled it'll be able to [send](https://brn1337.gitlab.io/dead-man-switch/) its share to the smart contract using any account.

When the minimum number of shares needed is reached the DMS will deactivate and the secret will be public. Using an additional share sent to all participant but never sent to the blockchain it is possible to keep the secret private (the number of shares sent to all partecipants must be calculated to avoid the decryption of the secret with less than the required number of third-party).

## Credits

- https://iancoleman.io/shamir/
- https://emn178.github.io/online-tools/keccak_256.html
- https://blog.keys.casa/shamirs-secret-sharing-security-shortcomings/
