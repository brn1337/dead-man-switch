// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4.0;

  /**
   * @title DeadManSwitch
   * @dev A dead-man-switch that activates once the minimum number of shares is received
   */


/*
Useful links
    https://iancoleman.io/shamir/
    https://emn178.github.io/online-tools/keccak_256.html
    https://blog.keys.casa/shamirs-secret-sharing-security-shortcomings/
*/

contract DeadManSwitch{

    event NewSwitch(uint uid, address owner, uint8 totShares, uint8 minShares);
    event ShareSent(uint uid, address owner, bytes share);
    event DeadSwitch(uint uid, address owner, bytes[] shares);

    struct Switch {
      bool dead;
      uint8 totShares;
      uint8 minShares;
      uint8 pubShares;
      bytes32[] sharesHash;
      bytes[] shares;
    }

    mapping(uint => address) public switchToOwner;
    mapping(address => uint[]) public ownerToSwitches;

    Switch[] public switches;

    function createSwitch(uint8 totShares, uint8 minShares, bytes32[] memory sharesHash) public {
        // check valid initialization values
        require(totShares > 1, "Total shares must be > 1");
        require(minShares <= totShares, "Minimum shares can't be more than total");
        require(minShares > 1, "Minimum shares too low");
        require(sharesHash.length == totShares, "sharesHash does not match the expected size");
        // create switch
        switches.push(Switch(false, totShares, minShares, 0, sharesHash, new bytes[](0)));
        uint switchId = switches.length - 1;
        switchToOwner[switchId] = msg.sender;
        ownerToSwitches[msg.sender].push(switchId);
        emit NewSwitch(switchId, msg.sender, totShares, minShares);
    }

    function submitShare(uint switchId, bytes memory share) public {
        require(switchToOwner[switchId] != address(0), "Switch does not exists");
        require(switches[switchId].dead == false, "Switch is dead");
        // check if share already submitted
        bool shareFound = false;
        for (uint i=0; i<switches[switchId].shares.length; i++) {
            if(keccak256(share) == keccak256(switches[switchId].shares[i])){
                shareFound = true;
                break;
            }
        }
        require(shareFound == false, "Share already submitted");
        // check if share hash match acceptables
        //bytes memory nonce = hex"1111"; // abi.encodePacked(address(this));
        //bytes32 hash = keccak256(abi.encodePacked(nonce, share));
        bytes32 hash = keccak256(abi.encodePacked(share));
        bool acceptable = false;
        for (uint i=0; i<switches[switchId].sharesHash.length; i++) {
            if(switches[switchId].sharesHash[i] == hash){
                acceptable = true;
                break;
            }
        }
        require(acceptable, "The share does not belong to this switch");
        // add share
        switches[switchId].shares.push(share);
        switches[switchId].pubShares++;
        emit ShareSent(switchId, switchToOwner[switchId], share);
        // if the minimum number of shares is present kill the switch
        if(switches[switchId].pubShares >= switches[switchId].minShares){
            switches[switchId].dead = true;
            emit DeadSwitch(switchId, switchToOwner[switchId], switches[switchId].shares);
        }
    }

    function isSwitchDead(uint switchId) public view returns(bool){
        require(switchToOwner[switchId] != address(0), "Switch does not exists");
        return switches[switchId].dead;
    }

    function getShares(uint switchId) public view returns(bytes[] memory){
        require(switchToOwner[switchId] != address(0), "Switch does not exists");
        return switches[switchId].shares;
    }

    function getSwitchesByOwner(address _owner) public view returns(uint[] memory){
        return ownerToSwitches[_owner];
    }


}
